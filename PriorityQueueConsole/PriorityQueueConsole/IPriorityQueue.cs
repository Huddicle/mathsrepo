﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityQueueConsole
{
    interface IPriorityQueue
    {
        // adds an item with the given priority to the queue
        bool insert(int priority);

        // removes and return the item with the highest priority
        int remove_max();

        // returns true if there are no items in the queue
        bool isEmpty();
    }
}
