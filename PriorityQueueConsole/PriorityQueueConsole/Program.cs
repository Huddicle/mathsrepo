﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityQueueConsole
{
    class Program
    {

        static void Main(string[] args)
        {
            PriorityQueue priorityQueue = new PriorityQueue();

            priorityQueue.insert(0);
            priorityQueue.insert(1);
            priorityQueue.insert(2);
            priorityQueue.insert(1);
            priorityQueue.insert(6);
            priorityQueue.insert(4);
            priorityQueue.insert(9);
            priorityQueue.insert(8);

            Console.WriteLine(priorityQueue.remove_max());
            Console.WriteLine(priorityQueue.remove_max());
            Console.WriteLine(priorityQueue.remove_max());
            Console.WriteLine(priorityQueue.remove_max());
            Console.WriteLine(priorityQueue.remove_max());
            Console.WriteLine(priorityQueue.remove_max());
            Console.WriteLine(priorityQueue.remove_max());
            Console.WriteLine(priorityQueue.remove_max());
            Console.ReadLine();

        }
    }
}
