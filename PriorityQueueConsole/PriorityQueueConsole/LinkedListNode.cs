﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityQueueConsole
{
    class LinkedListNode
    {
        public int priority;
        public LinkedListNode next;
    }
}
