﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityQueueConsole
{
    class PriorityQueue : IPriorityQueue
    {
        LinkedListNode head;
        
        public PriorityQueue()
        {

        }

        public bool insert(int priority)
        {
            if (head == null)
            {
                head = new LinkedListNode();
                head.priority = priority;
            }
            else
            {
                LinkedListNode newNode = new LinkedListNode();
                newNode.priority = priority;
                LinkedListNode tempNode = head;
                head = newNode;
                head.next = tempNode;
            }

            return true;
        }

        public bool isEmpty()
        {
            if (head == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int remove_max()
        {
            LinkedListNode currentNode = head;
            int priority = -1;
            while (currentNode != null)
            {
                if (priority < currentNode.priority)
                {
                    priority = currentNode.priority;
                }
                currentNode = currentNode.next;
            }

            if (head != null)
            {
                LinkedListNode previousNode = head;
                currentNode = head.next;

                if (head.priority == priority)
                {
                    head = currentNode;
                    return priority;
                }

                while(currentNode != null)
                {
                    if (currentNode.priority == priority)
                    {
                        previousNode.next = currentNode.next;
                        return priority;
                    }
                    previousNode = currentNode;
                    currentNode = currentNode.next;
                }
            }

            return priority;
        }
    }
}
